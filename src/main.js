import Vue from 'vue'
import Start from './componentes/start.vue'

Vue.config.productionTip = true;

new Vue({
  render: h => h(Start),
}).$mount('#app');